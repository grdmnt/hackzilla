Rails.application.routes.draw do
  get 'home/index'
  get '/verify' => 'static_pages#verify'
  root 'static_pages#index'
  get '/projects/unfilled' => 'project#unfilled'
  get '/projects/under_bidding' => 'project#under_bidding'
  get '/projects/granted' => 'project#granted'
  get '/projects/ongoing' => 'project#ongoing'
  get '/projects/finished' => 'project#finished'
  get '/projects/potential' => 'project#potential'
  get '/projects/under_bidding' => 'static_pages#under_bidding'
  get '/projects/granted' => 'static_pages#granted'
  get '/projects/ongoing' => 'static_pages#ongoing'
  get '/projects/finished' => 'static_pages#finished'
  post '/projects/potential/accept' => 'project#accept_project'

  get '/workers/' => 'worker#pool'
  get '/projects/listing/:id' => 'project#listing'

  get '/pool' => 'static_pages#pool'
  get '/project/pool' => 'static_pages#projectpool'
  get '/registration' => 'static_pages#registration'
  #get 'static_pages/pool' => root 'static_pages#index'

  post '/sms/receive_text' => 'sms#receive_text'
  get '/sms/subscribe' => 'sms#subscribe'
  # post '/sms/subscribe' => 'sms#subscribe'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end