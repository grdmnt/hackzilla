class AddWorkerToSubscription < ActiveRecord::Migration[5.1]
  def change
    add_reference :subscriptions, :worker, foreign_key: true
  end
end
