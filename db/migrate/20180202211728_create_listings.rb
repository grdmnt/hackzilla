class CreateListings < ActiveRecord::Migration[5.1]
  def change
    create_table :listings do |t|
      t.integer :worker_count

      t.timestamps
    end
  end
end
