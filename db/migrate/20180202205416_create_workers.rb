class CreateWorkers < ActiveRecord::Migration[5.1]
  def change
    create_table :workers do |t|
      t.string :name
      t.string :skill
      t.text :biodata
      t.string :location
      t.boolean :verified

      t.timestamps
    end
  end
end
