class AddListingToWorkers < ActiveRecord::Migration[5.1]
  def change
    add_reference :workers, :listing, foreign_key: true
  end
end
