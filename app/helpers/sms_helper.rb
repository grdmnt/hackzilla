module SmsHelper
    def self.send(access_token, address, message)
        @urlstring_to_post = "https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/4787/requests?access_token=" << access_token
        @result = HTTParty.post(@urlstring_to_post.to_str, 
            :body => { :outboundSMSMessageRequest => {
                            :senderAddress => "4787",
                            :outboundSMSTextMessage => {:message => message},
                            :address => address
                                                     }
                     }.to_json,
            :headers => { 'Content-Type' => 'application/json' } )
    end
    def self.parse(params)
        if params["access_token"] && params["subscriber_number"]
            return params["access_token"],params["subscriber_number"]
        else
            pattern = params["inboundSMSMessageList"]["inboundSMSMessage"][0]["message"].gsub(/\s+/m, ' ').strip.split(" ")
            if pattern[0] == "REG" && pattern[1] && pattern[2] && pattern.length == 3
                pattern << params['inboundSMSMessageList']['inboundSMSMessage'][0]['senderAddress'][7,12]
                return "registration",pattern
            end
            if pattern[0] == "APPLY" && pattern[1]
                pattern << params['inboundSMSMessageList']['inboundSMSMessage'][0]['senderAddress'][7,12]
                return "application",pattern
            end
        end
        return false
    end
end
