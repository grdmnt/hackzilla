class Worker < ApplicationRecord
  has_one :subscription
  belongs_to :listing, optional: true
end
