class Subscription < ApplicationRecord
  belongs_to :worker, optional: true
end
