class SmsController < ApplicationController
 skip_before_action :verify_authenticity_token 
 protect_from_forgery prepend: true
  def receive_text
    # Pass subscription to SMSHelper
    list = SmsHelper.parse(params)
    if list[0] == 'registration'
      @subscription = Subscription.find_by(subscriber_number: list[1][3])
      if @subscription.worker.nil?
        @worker = Worker.new()
        @worker.name = list[1][1]
        @worker.skill = list[1][2]
        @worker.verified = false
        @worker.subscription = @subscription
        @worker.save
        SmsHelper.send(@subscription.access_token,@subscription.subscriber_number, 'Thank you for registering please verify')
      else
        SmsHelper.send(@subscription.access_token,@subscription.subscriber_number, 'Already registered, please verify')
      end
    elsif list[0] == 'application'
      @listing = Listing.find_by(id: list[1][1].to_i)
      @subscription = Subscription.find_by(subscriber_number: list[1][2])
      if @subscription.worker.nil?
        SmsHelper.send(@subscription.access_token,@subscription.subscriber_number, 'Not yet registered')
      elsif !@subscription.worker.verified
        SmsHelper.send(@subscription.access_token,@subscription.subscriber_number, 'Not yet verified')
      else
        # Valid listing id
        if @listing
          # if @listing.workers.count < @listing.worker_count
            @listing.workers << @subscription.worker
            SmsHelper.send(@subscription.access_token,@subscription.subscriber_number, 'Thank you for applying')
          # end

          # If listing is full, bid
          if @listing.workers.count == @listing.worker_count
            SmsHelper.send(@subscription.access_token,@subscription.subscriber_number, 'Now bidding')
            @project = @listing.project
            @project.state = 'under_bidding'
            @project.save
            # Bid
          end

        # No such listing was found
        else
          SmsHelper.send(@subscription.access_token,@subscription.subscriber_number, 'No such job')
        end
      end
    end
  end

  def text_blast
    # Provide location and skill
    # Use SMSHelper to send text
  end

  def subscribe
    if Subscription.where(access_token: params['access_token']).none?
      Subscription.create(access_token: params['access_token'],
        subscriber_number: params['subscriber_number'])
    end
    render 'home/index'
  end
end
